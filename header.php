<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php bloginfo('name')?></title>
	<link href="https://fonts.googleapis.com/css?family=Dancing+Script" rel="stylesheet">
	<?php wp_head(); ?>
</head>
<body>

		<div id="header">
			<div class="container clearfix">
			<div class="logo"><a class="logo-size" href="<?php echo home_url(); ?>"><?php bloginfo('name')?></a></div>
<!--
			<?php wp_nav_menu(array('theme_location'=>'menu', 'menu_class'=>'nav', 'container'=>'false')); ?>
-->

			<ul class="nav">

				<li><a href="#">Home</a></li>
				<li><a href="#services">Services</a></li>
				<li><a href="#portfolio">Portfolio</a></li>
				<li><a href="#about">About</a></li>
				<li><a href="#contact">Contact</a></li>
		<!--		<?php 
				$massiv_parametrov = array(
				'container' => false,
				'echo' => false,
				'items_wrap' => '%3$s',
				'depth' => 0	
				);

				print strip_tags(wp_nav_menu($massiv_parametrov), '<a>');
				 ?>  -->
			</ul> 
			</div>
		</div>
		<div id="home">
			<div class="container">

				<h3>SSSWelcome to us!</h3>
				<h1>It's nice to meet you</h1>
				<buttom class="btn">See more</buttom>
			</div>
		</div>