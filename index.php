<?php get_header();?>
<a name="services"></a>
		<div id="services" class="box">
			<div class="container">

				<h2 class="title">Services</h2>
					<p class="descr">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean 	
					</p>
						<div class="services-wrap clearfix">
							<div class="services-item commerce">
								<h5 class="title">E-commerce</h5>
								<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </p>
							</div>
							<div class="services-item responsive">
								<h5 class="title">Responsive Web</h5>
								<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </p>
							</div>
							<div class="services-item security">
								<h5 class="title">Web Security</h5>
								<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </p>
							</div>
						</div>
			</div>
		</div>

		<div id="portfolio" class="box">
		<a name="portfolio"></a>
			<div class="container">
				<h2 class="title">Portfolio</h2>
					<p class="descr">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean 	
					</p>
						<div class="portfolio-wrap clearfix">
						<div class="portfolio-item">
							<a href=""><img src="<?php bloginfo('template_url') ?>/img/port1.jpg" alt="img1" height="300" width="290"></a>
							<div class="p-item-footer">
								<h5 class="title">Ebony & Ivory</h5>
								<span>Branding</span>
							</div>
						</div>
						<div class="portfolio-item">
							<a href=""><img src="<?php bloginfo('template_url') ?>/img/port2.jpg" alt="img2" height="300" width="290"></a>
							<div class="p-item-footer">
								<h5 class="title">Smart Stationary</h5>
								<span>Print Design</span>
							</div>
						</div>
						<div class="portfolio-item">
							<a href=""><img src="<?php bloginfo('template_url') ?>/img/port3.jpg" alt="img3" height="300" width="290"></a>
							<div class="p-item-footer">
								<h5 class="title">Print Design</h5>
								<span>Branding</span>
							</div>
						</div>
						<div class="portfolio-item">
							<a href=""><img src="<?php bloginfo('template_url') ?>/img/port4.jpg" alt="img4" height="300" width="290"></a>
							<div class="p-item-footer">
								<h5 class="title">Vinyl Record</h5>
								<span>Branding</span>
							</div>
						</div>
						<div class="portfolio-item">
							<a href=""><img src="<?php bloginfo('template_url') ?>/img/port5.jpg" alt="img5" height="300" width="290"></a>
							<div class="p-item-footer">
								<h5 class="title">Treehouse Template</h5>
								<span>Branding</span>
							</div>
						</div>
						<div class="portfolio-item">
							<a href=""><img src="<?php bloginfo('template_url') ?>/img/port6.jpg" alt="img6" height="300" width="290"></a>
							<div class="p-item-footer">
								<h5 class="title">Burned Logo</h5>
								<span>Branding</span>
							</div>
						</div>
							
						</div>
			</div>
		</div>


				<div id="about" class="box">
				<a name="about"></a>
					<div class="container">
						<h2 class="title">About us</h2>
						<p class="descr">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean</p>
						<div class="about-wrap">
							<?php 
								if ( have_posts() ) : 
									$i = 0; 
									$k = 0;
									while ( have_posts() ) :
									the_post(); ?>
										<div class="about-item clearfix">
											<div class="about-text 
											<?=(++$i % 2) 
												? 'left text-right'
												: 'right text-left';
											?>"><h5 class="title">
												<span>
													<?php the_date('F Y');?>
												</span>
												<br>
													<?php the_title();?>
											</h5>
											<?php the_content();  ?>
											</div>
											<div class="about-img <?=(++$k % 2) 
												? 'left'
												: 'right';
												?>">
												<?php the_post_thumbnail('full'); ?>
											</div>
										</div>
									<?php endwhile;?>
								<?php else:  ?>
									nothing to view
								<?php endif;  ?>
								<a href="#" class="about-link">Our<br /> Story<br /> Continues<br /> ... </a>
						</div>
					</div>
				</div>


	<div id="team" class="box">

						<div class="container">
							<h2 class="title">Our amazing team</h2>
								<p class="descr">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean 	
								</p>
								<div class="team-wrap clearfix">
									<div class="team-item">
										<img src="<?php bloginfo('template_url') ?>/img/team1.jpg" width="200" height="220">
										<h5 class="title">Kimberly Thompson</h5>
										<p class="cat">Marketer</p>
										<div class="social">
											<a href="#" class="vk">&nbsp;</a>
											<a href="#" class="fb">&nbsp;</a>
											<a href="#" class="inst">&nbsp;</a>
										</div>
									</div>
									<div class="team-item">
										<img src="<?php bloginfo('template_url') ?>/img/team2.jpg" width="200" height="220">
										<h5 class="title">Rica Massimo</h5>
										<p class="cat">Coder</p>
										<div class="social">
											<a href="#" class="vk">&nbsp;</a>
											<a href="#" class="fb">&nbsp;</a>
											<a href="#" class="inst">&nbsp;</a>
										</div>
									</div>
									<div class="team-item">
										<img src="<?php bloginfo('template_url') ?>/img/team3.jpg" width="200" height="220">
										<h5 class="title">Uku Mason</h5>
										<p class="cat">Graphic Designer</p>
										<div class="social">
											<a href="#" class="vk">&nbsp;</a>
											<a href="#" class="fb">&nbsp;</a>
											<a href="#" class="inst">&nbsp;</a>
											</div>
										</div>
									</div>

								</div>
									<p class="descr team-info">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.<br /> Aenean. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean 	
								</p> 

								</div>
							
								</div>
							<div id="brand">
								<div class="container">
									<a href="#"><img src="<?php bloginfo('template_url') ?>/img/logo/imag1.png" alt="" width="70" height="70"></a>
									<a href="#"><img src="<?php bloginfo('template_url') ?>/img/logo/imag2.png" alt="" width="70" height="70"></a>
									<a href="#"><img src="<?php bloginfo('template_url') ?>/img/logo/imag3.png" alt="" width="70" height="70"></a>
									<a href="#"><img src="<?php bloginfo('template_url') ?>/img/logo/imag4.png" alt="" width="70" height="70"></a>
								</div>
							</div>

							<div id="contact" class="box">
								<a name="contact"></a>
								<div class="container">
									<h2 class="title">Contact us</h2>
										<p class="descr">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean 	
										</p>
										<form action="#" id="message">
										<div class="clearfix">
											<div class="left">
												<input type="text" name="" placeholder="Your name">
												<input type="text" name="" placeholder="Your e-mail">
												<input type="text" name="" placeholder="Subject">
											</div>
											<div class="right">
												<textarea cols="0" rows="0" placeholder="Your message"></textarea>
											</div>
											</div>
											<input type="submit" value="Send message" class="btn">
										</form>
								</div>
							</div>
							
				<?php get_footer();  ?>			


